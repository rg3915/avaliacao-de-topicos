from django.db import models


class Produto(models.Model):
    titulo = models.CharField(max_length=100)
    categoria = models.ForeignKey(
        'Categoria',
        on_delete=models.SET_NULL,
        related_name='produtos',
        null=True,
        blank=True
    )

    class Meta:
        ordering = ('titulo',)
        verbose_name = 'produto'
        verbose_name_plural = 'produtos'

    def __str__(self):
        return f'{self.titulo}'


class Categoria(models.Model):
    titulo = models.CharField(max_length=100)

    class Meta:
        ordering = ('titulo',)
        verbose_name = 'categoria'
        verbose_name_plural = 'categorias'

    def __str__(self):
        return f'{self.titulo}'


class Subcategoria(models.Model):
    titulo = models.CharField(max_length=100, unique=True)
    categoria = models.ForeignKey(
        Categoria,
        on_delete=models.SET_NULL,
        related_name='subcategorias',
        null=True,
        blank=True
    )

    class Meta:
        ordering = ('titulo',)
        verbose_name = 'subcategoria'
        verbose_name_plural = 'subcategorias'

    def __str__(self):
        return f'{self.titulo}'


class Topico(models.Model):
    titulo = models.CharField(max_length=100)
    peso = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)  # noqa E501
    subcategoria = models.ForeignKey(
        Subcategoria,
        on_delete=models.SET_NULL,
        related_name='topicos',
        null=True,
        blank=True
    )

    class Meta:
        ordering = ('titulo',)
        verbose_name = 'tópico'
        verbose_name_plural = 'tópicos'

    def __str__(self):
        return f'{self.titulo}'


class Nota(models.Model):
    titulo = models.CharField(max_length=100, unique=True)
    data = models.DateTimeField(auto_now_add=True)
    produto = models.ForeignKey(
        Produto,
        on_delete=models.SET_NULL,
        related_name='produto_itens',
        null=True,
        blank=True
    )

    class Meta:
        ordering = ('titulo',)
        verbose_name = 'nota'
        verbose_name_plural = 'notas'

    def __str__(self):
        return f'{self.titulo}'


class NotaItens(models.Model):
    nota = models.ForeignKey(
        Nota,
        on_delete=models.SET_NULL,
        related_name='notas',
        null=True,
        blank=True
    )
    topico = models.ForeignKey(
        Topico,
        on_delete=models.SET_NULL,
        related_name='topico_itens',
        null=True,
        blank=True
    )
    valor = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)  # noqa E501

    class Meta:
        ordering = ('pk',)
        verbose_name = 'item'
        verbose_name_plural = 'itens'

    def __str__(self):
        return f'{self.pk}'
