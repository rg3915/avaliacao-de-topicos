# Generated by Django 2.2.12 on 2022-06-25 23:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20220625_2047'),
    ]

    operations = [
        migrations.CreateModel(
            name='Nota',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'nota',
                'verbose_name_plural': 'notas',
                'ordering': ('data',),
            },
        ),
        migrations.CreateModel(
            name='NotaItens',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('valor', models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True)),
                ('nota', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='notas', to='core.Nota')),
                ('produto', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='produto_itens', to='core.Produto')),
                ('topico', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='topico_itens', to='core.Topico')),
            ],
            options={
                'verbose_name': 'item',
                'verbose_name_plural': 'itens',
                'ordering': ('pk',),
            },
        ),
        migrations.DeleteModel(
            name='Itens',
        ),
    ]
