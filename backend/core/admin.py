from django.contrib import admin
from .models import Produto, Categoria, Subcategoria, Topico, Nota, NotaItens
from django.shortcuts import redirect
from django.urls import reverse_lazy


@admin.register(Produto)
class ProdutoAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'categoria')


@admin.register(Subcategoria)
class SubcategoriaAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'categoria')


@admin.register(Topico)
class TopicoAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'peso', 'subcategoria')


class NotaItensInline(admin.TabularInline):
    model = NotaItens
    readonly_fields = ('topico',)
    extra = 0

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Nota)
class NotaAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'data')
    inlines = (NotaItensInline,)

    def save_model(self, request, obj, form, change):
        if not change:
            subcategorias = obj.produto.categoria.subcategorias.all()
            topicos = Topico.objects.filter(subcategoria__in=subcategorias)
            obj.save()

            for topico in topicos:
                NotaItens.objects.create(nota=obj, topico=topico)

        super(NotaAdmin, self).save_model(request, obj, form, change)

    def response_add(self, request, obj, post_url_continue=None):
        return redirect(reverse_lazy('admin:core_nota_change', kwargs={'object_id': obj.id}))  # noqa E501


admin.site.register(Categoria)
