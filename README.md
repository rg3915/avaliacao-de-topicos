# Avaliação de Tópicos

Nota de produtos agrupados por categoria.

## Este projeto foi feito com:

* [Python 3.10.4](https://www.python.org/)
* [Django 4.0.5](https://www.djangoproject.com/)
* [Bootstrap 4.0](https://getbootstrap.com/)
* [htmx](https://htmx.org)

## Como rodar o projeto?

* Clone esse repositório.
* Crie um virtualenv com Python 3.
* Ative o virtualenv.
* Instale as dependências.
* Rode as migrações.

```
git clone https://gitlab.com/rg3915/avaliacao-de-topicos.git
cd avaliacao-de-topicos
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
python contrib/env_gen.py
python manage.py migrate
python manage.py createsuperuser --username="admin" --email=""
```

## Diagrama

![](img/core.png)

https://www.dicas-de-django.com.br/36-django-visualizando-seus-modelos-com-graph-models

